(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["news-news-module"],{

/***/ "./node_modules/ngx-xml2json/fesm2015/ngx-xml2json.js":
/*!************************************************************!*\
  !*** ./node_modules/ngx-xml2json/fesm2015/ngx-xml2json.js ***!
  \************************************************************/
/*! exports provided: NgxXml2jsonService, NgxXml2jsonComponent, NgxXml2jsonModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxXml2jsonService", function() { return NgxXml2jsonService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxXml2jsonComponent", function() { return NgxXml2jsonComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxXml2jsonModule", function() { return NgxXml2jsonModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NgxXml2jsonService {
    constructor() { }
    /**
     * @param {?} xml
     * @return {?}
     */
    xmlToJson(xml) {
        let /** @type {?} */ obj = {};
        if (xml.nodeType === 1) {
            if (xml.attributes.length > 0) {
                obj['@attributes'] = {};
                for (let /** @type {?} */ j = 0; j < xml.attributes.length; j += 1) {
                    const /** @type {?} */ attribute = xml.attributes.item(j);
                    obj['@attributes'][attribute.nodeName] = attribute.nodeValue;
                }
            }
        }
        else if (xml.nodeType === 3) {
            obj = xml.nodeValue;
        }
        if (xml.hasChildNodes() && xml.childNodes.length === 1 && xml.childNodes[0].nodeType === 3) {
            obj = xml.childNodes[0].nodeValue;
        }
        else if (xml.hasChildNodes()) {
            for (let /** @type {?} */ i = 0; i < xml.childNodes.length; i += 1) {
                const /** @type {?} */ item = xml.childNodes.item(i);
                const /** @type {?} */ nodeName = item.nodeName;
                if (typeof (obj[nodeName]) === 'undefined') {
                    obj[nodeName] = this.xmlToJson(item);
                }
                else {
                    if (typeof (obj[nodeName].push) === 'undefined') {
                        const /** @type {?} */ old = obj[nodeName];
                        obj[nodeName] = [];
                        obj[nodeName].push(old);
                    }
                    obj[nodeName].push(this.xmlToJson(item));
                }
            }
        }
        return obj;
    }
}
NgxXml2jsonService.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"], args: [{
                providedIn: 'root'
            },] },
];
/** @nocollapse */
NgxXml2jsonService.ctorParameters = () => [];
/** @nocollapse */ NgxXml2jsonService.ngInjectableDef = Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["defineInjectable"])({ factory: function NgxXml2jsonService_Factory() { return new NgxXml2jsonService(); }, token: NgxXml2jsonService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NgxXml2jsonComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
NgxXml2jsonComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: 'lib-ngx-xml2json',
                template: `
    <p>
      Ngx-xml2json
    </p>
  `,
                styles: []
            },] },
];
/** @nocollapse */
NgxXml2jsonComponent.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NgxXml2jsonModule {
}
NgxXml2jsonModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                imports: [],
                declarations: [NgxXml2jsonComponent],
                exports: [NgxXml2jsonComponent]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */



//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmd4LXhtbDJqc29uLmpzLm1hcCIsInNvdXJjZXMiOlsibmc6Ly9uZ3gteG1sMmpzb24vbGliL25neC14bWwyanNvbi5zZXJ2aWNlLnRzIiwibmc6Ly9uZ3gteG1sMmpzb24vbGliL25neC14bWwyanNvbi5jb21wb25lbnQudHMiLCJuZzovL25neC14bWwyanNvbi9saWIvbmd4LXhtbDJqc29uLm1vZHVsZS50cyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIE5neFhtbDJqc29uU2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICB4bWxUb0pzb24oeG1sKSB7XG5cbiAgICBsZXQgb2JqID0ge307XG5cbiAgICBpZiAoeG1sLm5vZGVUeXBlID09PSAxKSB7XG4gICAgICBpZiAoeG1sLmF0dHJpYnV0ZXMubGVuZ3RoID4gMCkge1xuICAgICAgICBvYmpbJ0BhdHRyaWJ1dGVzJ10gPSB7fTtcbiAgICAgICAgZm9yIChsZXQgaiA9IDA7IGogPCB4bWwuYXR0cmlidXRlcy5sZW5ndGg7IGogKz0gMSkge1xuICAgICAgICAgIGNvbnN0IGF0dHJpYnV0ZSA9IHhtbC5hdHRyaWJ1dGVzLml0ZW0oaik7XG4gICAgICAgICAgb2JqWydAYXR0cmlidXRlcyddW2F0dHJpYnV0ZS5ub2RlTmFtZV0gPSBhdHRyaWJ1dGUubm9kZVZhbHVlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSBlbHNlIGlmICh4bWwubm9kZVR5cGUgPT09IDMpIHtcbiAgICAgIG9iaiA9IHhtbC5ub2RlVmFsdWU7XG4gICAgfVxuXG4gICAgaWYgKHhtbC5oYXNDaGlsZE5vZGVzKCkgJiYgeG1sLmNoaWxkTm9kZXMubGVuZ3RoID09PSAxICYmIHhtbC5jaGlsZE5vZGVzWzBdLm5vZGVUeXBlID09PSAzKSB7XG4gICAgICBvYmogPSB4bWwuY2hpbGROb2Rlc1swXS5ub2RlVmFsdWU7XG4gICAgfSBlbHNlIGlmICh4bWwuaGFzQ2hpbGROb2RlcygpKSB7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHhtbC5jaGlsZE5vZGVzLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICAgIGNvbnN0IGl0ZW0gPSB4bWwuY2hpbGROb2Rlcy5pdGVtKGkpO1xuICAgICAgICBjb25zdCBub2RlTmFtZSA9IGl0ZW0ubm9kZU5hbWU7XG4gICAgICAgIGlmICh0eXBlb2YgKG9ialtub2RlTmFtZV0pID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgIG9ialtub2RlTmFtZV0gPSB0aGlzLnhtbFRvSnNvbihpdGVtKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpZiAodHlwZW9mIChvYmpbbm9kZU5hbWVdLnB1c2gpID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgY29uc3Qgb2xkID0gb2JqW25vZGVOYW1lXTtcbiAgICAgICAgICAgIG9ialtub2RlTmFtZV0gPSBbXTtcbiAgICAgICAgICAgIG9ialtub2RlTmFtZV0ucHVzaChvbGQpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBvYmpbbm9kZU5hbWVdLnB1c2godGhpcy54bWxUb0pzb24oaXRlbSkpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBvYmo7XG4gIH1cblxufVxuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLW5neC14bWwyanNvbicsXG4gIHRlbXBsYXRlOiBgXG4gICAgPHA+XG4gICAgICBOZ3gteG1sMmpzb25cbiAgICA8L3A+XG4gIGAsXG4gIHN0eWxlczogW11cbn0pXG5leHBvcnQgY2xhc3MgTmd4WG1sMmpzb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG59XG4iLCJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmd4WG1sMmpzb25Db21wb25lbnQgfSBmcm9tICcuL25neC14bWwyanNvbi5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW05neFhtbDJqc29uQ29tcG9uZW50XSxcbiAgZXhwb3J0czogW05neFhtbDJqc29uQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBOZ3hYbWwyanNvbk1vZHVsZSB7IH1cbiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtJQU9FLGlCQUFpQjs7Ozs7SUFFakIsU0FBUyxDQUFDLEdBQUc7UUFFWCxxQkFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDO1FBRWIsSUFBSSxHQUFHLENBQUMsUUFBUSxLQUFLLENBQUMsRUFBRTtZQUN0QixJQUFJLEdBQUcsQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDN0IsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDeEIsS0FBSyxxQkFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNqRCx1QkFBTSxTQUFTLEdBQUcsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3pDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQztpQkFDOUQ7YUFDRjtTQUNGO2FBQU0sSUFBSSxHQUFHLENBQUMsUUFBUSxLQUFLLENBQUMsRUFBRTtZQUM3QixHQUFHLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQztTQUNyQjtRQUVELElBQUksR0FBRyxDQUFDLGFBQWEsRUFBRSxJQUFJLEdBQUcsQ0FBQyxVQUFVLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsS0FBSyxDQUFDLEVBQUU7WUFDMUYsR0FBRyxHQUFHLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO1NBQ25DO2FBQU0sSUFBSSxHQUFHLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDOUIsS0FBSyxxQkFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNqRCx1QkFBTSxJQUFJLEdBQUcsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BDLHVCQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUMvQixJQUFJLFFBQVEsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssV0FBVyxFQUFFO29CQUMxQyxHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDdEM7cUJBQU07b0JBQ0wsSUFBSSxRQUFRLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxXQUFXLEVBQUU7d0JBQy9DLHVCQUFNLEdBQUcsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQzFCLEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUM7d0JBQ25CLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQ3pCO29CQUNELEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUMxQzthQUNGO1NBQ0Y7UUFDRCxPQUFPLEdBQUcsQ0FBQztLQUNaOzs7WUExQ0YsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7Ozs7Ozs7O0FDSkQ7SUFhRSxpQkFBaUI7Ozs7SUFFakIsUUFBUSxNQUFLOzs7WUFiZCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsUUFBUSxFQUFFOzs7O0dBSVQ7Z0JBQ0QsTUFBTSxFQUFFLEVBQUU7YUFDWDs7Ozs7Ozs7O0FDVkQ7OztZQUdDLFFBQVEsU0FBQztnQkFDUixPQUFPLEVBQUUsRUFDUjtnQkFDRCxZQUFZLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztnQkFDcEMsT0FBTyxFQUFFLENBQUMsb0JBQW9CLENBQUM7YUFDaEM7Ozs7Ozs7Ozs7Ozs7OzsifQ==

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/news/news.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/news/news.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <div style=\"width:100%; height:15px;background-image: url('assets/images/mobile-bg-topbar.png');margin-left:0px; \">\n    </div>\n    <table style=\"width:100%;text-align: left\">\n      <tr>\n        <td style=\"width:33%;\">\n          <ion-buttons slot=\"start\">\n            <ion-icon slot=\"icon-only\" style=\"color:dodgerblue;\" name=\"arrow-round-back\" role=\"img\" aria-label=\"star\"\n              routerLink=\"/home\" routerDirection=\"root\"></ion-icon>\n          </ion-buttons>\n        </td>\n        <td>\n          <img src=\"assets/images/mobile-logo-myjax.png\" />\n\n        </td>\n      </tr>\n    </table>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content style=\"text-align: center; background-color: lightblue\">\n  <b><label style=\"font-size:20px;\">News</label></b>\n  <ion-list >\n    <ion-item *ngFor=\"let newsList of newsLists \" (click)=\"openlink(newsList.link)\" >\n      <table style=\"width:100%;\">\n        <tr>\n          <td style=\"width:30%;\">\n          <p>{{newsList.title}}</p>           \n          </td>\n          <td style=\"width:60%;\">\n          <div style=\"margin-left:2%;\" [innerHTML]=\"newsList.description\"></div>\n          </td>\n          <td style=\"text-align: right;  width:10%;\">\n            <ion-icon name=\"arrow-dropright-circle\"></ion-icon>\n          </td>\n        </tr>\n      </table>\n    </ion-item>\n  </ion-list>\n\n</ion-content>"

/***/ }),

/***/ "./src/app/news/news.module.ts":
/*!*************************************!*\
  !*** ./src/app/news/news.module.ts ***!
  \*************************************/
/*! exports provided: NewsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsPageModule", function() { return NewsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _news_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./news.page */ "./src/app/news/news.page.ts");







const routes = [
    {
        path: '',
        component: _news_page__WEBPACK_IMPORTED_MODULE_6__["NewsPage"]
    }
];
let NewsPageModule = class NewsPageModule {
};
NewsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_news_page__WEBPACK_IMPORTED_MODULE_6__["NewsPage"]]
    })
], NewsPageModule);



/***/ }),

/***/ "./src/app/news/news.page.scss":
/*!*************************************!*\
  !*** ./src/app/news/news.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25ld3MvbmV3cy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/news/news.page.ts":
/*!***********************************!*\
  !*** ./src/app/news/news.page.ts ***!
  \***********************************/
/*! exports provided: NewsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsPage", function() { return NewsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _my_jax_services_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../my-jax-services.service */ "./src/app/my-jax-services.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var ngx_xml2json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-xml2json */ "./node_modules/ngx-xml2json/fesm2015/ngx-xml2json.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/http/ngx */ "./node_modules/@ionic-native/http/ngx/index.js");







let NewsPage = class NewsPage {
    constructor(myJaxService, http, ngxXml2jsonService, iab, HTTP) {
        this.myJaxService = myJaxService;
        this.http = http;
        this.ngxXml2jsonService = ngxXml2jsonService;
        this.iab = iab;
        this.HTTP = HTTP;
        this.newsLists = [];
    }
    ngOnInit() {
        this.callXmlData();
    }
    // Below are actual and dummy call, comment te method
    /// The One with actual url---> 
    //https://cityofjacksonville.net/RSSFeed.aspx?ModID=76&CID=All-0
    callXmlData() {
        this.HTTP.get('https://www.coj.net/CMSPages/coj_newsRss25.aspx', { responseType: '' }, {})
            .then(data => {
            // alert("in data"+data.status);
            // alert("in data"+data.data); // data received by server
            // alert("in data"+data.headers);
            const parser = new DOMParser();
            const xml = parser.parseFromString(data.data, 'text/xml');
            // const obj: any = this.ngxXml2jsonService.xmlToJson(xml);
            let x = xml.getElementsByTagName("title");
            var list = [];
            for (let i = 0; i < x.length; i++) {
                var obj = {};
                obj["title"] = xml.getElementsByTagName("title")[i].childNodes[0].nodeValue;
                obj["description"] = xml.getElementsByTagName("description")[i].childNodes[0].nodeValue;
                obj["link"] = xml.getElementsByTagName("link")[i].childNodes[0].nodeValue;
                list.push(obj);
            }
            this.newsLists = list;
            this.newsLists.shift();
        })
            .catch(error => {
            // error message as string--->   
            alert("err is:" + JSON.stringify(error.error));
        });
    }
    /// the working dummy one--->
    // callXmlData() {
    //   this.myJaxService.getXML()
    //     .subscribe(
    //       (data: any) => {
    //         console.log(data);
    //         //  parse the Xml now-->
    //         const parser = new DOMParser();
    //         const xml = parser.parseFromString(data, 'text/xml');
    //         //   const obj: any = this.ngxXml2jsonService.xmlToJson(xml);
    //         //   this.newsLists = obj.rss.channel.item;
    //         let x = xml.getElementsByTagName("title");
    // var list = [];
    // for (let i = 0; i < x.length; i++) {
    //   var obj = {};
    //   obj["title"] = xml.getElementsByTagName("title")[i].childNodes[0].nodeValue;
    //   obj["description"] = xml.getElementsByTagName("description")[i].childNodes[0].nodeValue;
    //   obj["link"] = xml.getElementsByTagName("link")[i].childNodes[0].nodeValue;
    //   list.push(obj);
    // }
    // this.newsLists = list;
    //         
    //       },
    //       (err: any) => { alert("err is:" + JSON.stringify(err)); }
    //     );
    // }
    openlink(link) {
        this.iab.create(link, `_system`, { location: 'yes', zoom: 'no' });
    }
};
NewsPage.ctorParameters = () => [
    { type: _my_jax_services_service__WEBPACK_IMPORTED_MODULE_2__["MyJaxServicesService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: ngx_xml2json__WEBPACK_IMPORTED_MODULE_4__["NgxXml2jsonService"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_5__["InAppBrowser"] },
    { type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_6__["HTTP"] }
];
NewsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-news',
        template: __webpack_require__(/*! raw-loader!./news.page.html */ "./node_modules/raw-loader/index.js!./src/app/news/news.page.html"),
        styles: [__webpack_require__(/*! ./news.page.scss */ "./src/app/news/news.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_my_jax_services_service__WEBPACK_IMPORTED_MODULE_2__["MyJaxServicesService"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
        ngx_xml2json__WEBPACK_IMPORTED_MODULE_4__["NgxXml2jsonService"],
        _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_5__["InAppBrowser"],
        _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_6__["HTTP"]])
], NewsPage);



/***/ })

}]);
//# sourceMappingURL=news-news-module-es2015.js.map