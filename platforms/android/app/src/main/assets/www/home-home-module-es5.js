(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header >\n  <ion-toolbar >\n      <div style=\"width:100%; height:15px;background-image: url('assets/images/mobile-bg-topbar.png');margin-left:0px; \">\n      </div>\n    <table style=\"width:100%;text-align: left\">\n      <tr>\n        <td style=\"width:33%;\">\n            <ion-buttons  slot=\"start\">\n                <ion-menu-button large style=\"color:dodgerblue;\"></ion-menu-button>\n              </ion-buttons>\n        </td>\n        <td >\n            <img src=\"assets/images/mobile-logo-myjax.png\"  />\n\n        </td>\n      </tr>\n    </table>  \n   \n  </ion-toolbar>\n</ion-header>\n\n<ion-content style=\"text-align: center;\">\n  <div  class=\"screenHeight\" style=\" background-image: url('assets/images/mobile-bg.png');\">\n\n    <div style=\"width:100%; height:60px;background-color: #E9E9E9;align-content: center;\">\n      <img style=\"margin-top:15px;margin-right:10px;display:inline-block;vertical-align: middle;\"\n        src=\"assets/images/mobile-icon-search.png\">\n        \n      <input style=\"margin-top:15px;display:inline-block;vertical-align: middle;border:1px solid#BDBDBD;\" type=\"text\"\n        placeholder=\"Search for answers\" [(ngModel)]=\"searchText\" (keyup.enter)=\"openSearch(searchText);\" />\n      <!-- <button style=\"margin-top:15px;margin-right:10px;display:inline-block;vertical-align: middle;\" (click)=\"openSearch(searchText);\">Ok</button> -->\n    </div>\n\n    <div>\n      <!-- <button style=\" margin-top:3%; background:transparent !important;color: #ffffff; \n     border:1px solid #FFFFFF; font-size: 25px;border-radius: 5px;\" (click)=\"openLogin();\" >Login or Sign Up</button>\n      <button style=\"display:none; margin-top:3%; background:transparent !important;color: #ffffff; \n      border:1px solid #FFFFFF; font-size: 25px;border-radius: 5px; \">Welcome Ginny!</button> -->\n      <button style=\"margin-top:3%; background:transparent !important;color: #ffffff; \n      border:1px solid #FFFFFF; font-size: 25px;border-radius: 5px; \">Welcome!</button>\n      <div style=\" margin-top:3%;\">\n          <div\n          style=\"vertical-align:middle;display:inline-block; width:40%; height:91px;margin-right:5px;background-image: url('assets/images/mobile-icon-xl-bg.png'); \">\n          <a style=\"text-decoration: none;\" (click)=\"openSubmitRequestReport();\">\n            <img style=\"margin-top:5px;\" src=\"assets/images/mobile-icon-submit.png\">\n            <label style=\"display: flex; flex-direction:column;font-size: 15px;color:black\"><b>Submit </b></label>\n            <label style=\"display: flex; flex-direction:column;font-size: 15px;color:black\"><b> Request/Report</b></label>\n          </a>\n        </div>\n        <div\n          style=\"vertical-align:middle;display:inline-block; width:40%; height:91px;margin-left:5px;background-image: url('assets/images/mobile-icon-xl-bg.png'); \">\n          <a style=\"text-decoration: none;margin-top:5px;margin-bottom: 5px;\" routerLink=\"/fight-blight\" routerDirection=\"root\">\n            <img  style=\"margin-top:5px;\" src=\"assets/images/mobile-icon-blight.png\">\n            <p style=\"font-size: 15px;color:black\"><b>Fight Blight</b></p>\n          </a>\n        </div>\n      </div>\n     \n\n      <div\n        style=\"text-align:left; background-image: url('assets/images/mobile-icon-bg.png'); margin-left:8%;margin-right:8%; margin-top:2%; \">\n        <a style=\"text-decoration: none;\" (click)=\"openCheckStatus();\">\n          <img style=\"vertical-align:middle;display:inline-block;margin-left:5%;\" src=\"assets/images/mobile-icon-requests.png\">\n          <p style=\"margin-left:5%; vertical-align:middle;display:inline-block;font-size: 15px;color:black\"><b>Check Status</b></p>\n        </a>\n      </div>\n      <div\n        style=\"text-align:left; background-image: url('assets/images/mobile-icon-bg.png'); margin-left:8%;margin-right:8%; margin-top:2%; \">\n        <a style=\"text-decoration: none;\" href=\"https://www.google.com/maps\">\n          <img style=\"vertical-align:middle;display:inline-block;margin-left:5%;\" src=\"assets/images/mobile-icon-around.png\">\n          <p style=\"margin-left:5%; vertical-align:middle;display:inline-block;font-size: 15px;color:black\"><b>What's Around Me</b></p>\n        </a>\n      </div>\n      <div\n      style=\"text-align:left; background-image: url('assets/images/mobile-icon-bg.png'); margin-left:8%;margin-right:8%; margin-top:2%; \">\n      <a style=\"text-decoration: none;\"  routerLink=\"/news\" routerDirection=\"root\">\n        <img style=\"vertical-align:middle;display:inline-block;margin-left:5%;\" src=\"assets/images/mobile-icon-news.png\">\n        <p style=\"margin-left:5%; vertical-align:middle;display:inline-block;font-size: 15px;color:black\"><b>News</b></p>\n      </a>\n    </div>\n      <div\n        style=\"text-align:left; background-image: url('assets/images/mobile-icon-bg.png'); margin-left:8%;margin-right:8%; margin-top:2%; \">\n        <a style=\"text-decoration: none;\" href=\"tel:+19046302489\">\n          <img style=\"vertical-align:middle;display:inline-block;margin-left:5%;\" src=\"assets/images/mobile-icon-call.png\">\n          <p style=\"margin-left:5%; vertical-align:middle;display:inline-block;font-size: 15px;color:black\"><b>Call Us</b></p>\n        </a>\n      </div>\n\n      <!-- <ion-button href=\"https://ionicframework.com/docs/\" class=\"button button-block  \"><a target=\"_blank\"></a>My\n        Requests</ion-button>\n      <br />\n      <ion-button href=\"https://www.google.com\" class=\"button button-block  \"><a target=\"_blank\"></a>What's around me\n      </ion-button>\n      <br />\n      <ion-button href=\"https://www.bbc.com/news\" class=\"button button-block  \"><a target=\"_blank\"></a>News</ion-button>\n      <br />\n      <ion-button class=\"button  \"><a href=\"tel:+12123334444\"></a>Call Us</ion-button>\n      <ion-button (click)=\"openBlank()\">Open Blank</ion-button> -->\n      <p style=\"color:white; text-shadow: #3A6A83; font-size: 15px;\"><b>Visit Us</b></p>\n      <table style=\"width:100%;align-items:center;\">\n        <tr>\n          <td style=\"text-align: right;\">\n            <a  href=\"https://www.facebook.com/CityofJax/\">\n              <img alt=\"W3Schools\" src=\"assets/images/mobile-icon-facebook.png\">\n            </a>\n\n          </td>\n          <td style=\"text-align: center;\">\n            <a  href=\"https://www.twitter.com/CityofJax \">\n              <img alt=\"W3Schools\" src=\"assets/images/mobile-icon-twitter.png\">\n            </a>\n\n          </td>\n          <td style=\"text-align: left;\">\n            <a  href=\"http://www.coj.net/departments/public-affairs/social-media.aspx\">\n              <img alt=\"W3Schools\" src=\"assets/images/mobile-icon-social.png\">\n            </a>\n          </td>\n        </tr>\n      </table>\n    </div>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");







var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card ion-img {\n  max-height: 35vh;\n  overflow: hidden;\n}\n\n.screenHeight {\n  width: 100%;\n  height: 100vh;\n  text-align: center;\n}\n\n@media (max-width: 350px) {\n  .screenHeight {\n    width: 100%;\n    height: 120vh;\n    text-align: center;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcVXNlcnNcXHNuaWdkaGFtXFxteVByb2plY3RzXFxDT0pcXG15SmF4L3NyY1xcYXBwXFxob21lXFxob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURDQTtFQUNFLFdBQUE7RUFBVyxhQUFBO0VBQWEsa0JBQUE7QUNJMUI7O0FEREc7RUFDQztJQUNDLFdBQUE7SUFBVyxhQUFBO0lBQWEsa0JBQUE7RUNNM0I7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud2VsY29tZS1jYXJkIGlvbi1pbWcge1xuICBtYXgtaGVpZ2h0OiAzNXZoO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLnNjcmVlbkhlaWdodCB7XG4gIHdpZHRoOjEwMCU7aGVpZ2h0OjEwMHZoO3RleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG5cbiAgIEBtZWRpYSAobWF4LXdpZHRoOiAzNTBweCkge1xuICAgIC5zY3JlZW5IZWlnaHQge1xuICAgICB3aWR0aDoxMDAlO2hlaWdodDoxMjB2aDt0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgfVxuICAgfVxuXG4gICIsIi53ZWxjb21lLWNhcmQgaW9uLWltZyB7XG4gIG1heC1oZWlnaHQ6IDM1dmg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5zY3JlZW5IZWlnaHQge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5AbWVkaWEgKG1heC13aWR0aDogMzUwcHgpIHtcbiAgLnNjcmVlbkhlaWdodCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMjB2aDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");



var HomePage = /** @class */ (function () {
    function HomePage(iab) {
        this.iab = iab;
    }
    // will open in an inapp browser--->
    HomePage.prototype.openLogin = function () {
        var browser = this.iab.create("https://opn-ast2.rightnowdemo.com/app/utils/login_form", "_blank", { toolbar: 'no', hideurlbar: 'yes', location: 'no' });
        browser.on('loadstart')
            .subscribe(function (event) {
            browser.executeScript({
                code: "alert(\"Hi\");\n        document.getElementById(\"rn_LoginForm_30_Submit\").submit(function() {\n          alert(\"Button code executed.\");\n        });\n         "
            });
            browser.show();
        });
        browser.on('loadstop')
            .subscribe(function (event) {
            browser.executeScript({
                code: " \n        alert(\"loadcomplete\");\n        document.getElementById(\"rn_LoginForm_30_Submit\").submit(function() {\n          alert(\"Button code executed.\");\n        });   \n        "
            });
            browser.show();
        });
        // 
        //   this.iab.on('loadstop').subscribe(event: any => {
        //     iab.insertCSS({ code: "body{color: red;" });
        //  });https://Praveen:Password@1@opn-ast2.rightnowdemo.com/ci/admin
    };
    HomePage.prototype.openSubmitRequestReport = function () {
        this.iab.create("https://myjax.custhelp.com/app/AllServices", "_system");
    };
    HomePage.prototype.openCheckStatus = function () {
        //https://Praveen:Password@1@opn-ast2.rightnowdemo.com/ci/admin
        this.iab.create("https://myjax.custhelp.com/app/search_sr", "_system", { hideurlbar: 'no' });
    };
    HomePage.prototype.openSearch = function (searchText) {
        // https://opn-ast2.rightnowdemo.com/app/results/kw/
        this.iab.create("https://myjax.custhelp.com/app/results/kw/" + searchText, "_system");
    };
    HomePage.prototype.whatsAroundMeClicked = function () {
        //https://www.google.com/maps/search/things+to+do+near+me
        //https://www.google.com/search?q=things+to+do+near+me
        this.iab.create("https://www.google.com/maps", "_system");
    };
    HomePage.ctorParameters = function () { return [
        { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__["InAppBrowser"] }
    ]; };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__["InAppBrowser"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module-es5.js.map