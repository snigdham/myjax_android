import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Login/Sign Up',
      url: '/home',
      icon: 'log-in'
    },    
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },    
    {
      title: 'About',
      url: '/about',
      icon: 'apps'
    }
    
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private iab: InAppBrowser
  ) {
    this.initializeApp();
  }
  clickCheck(page:any){
    if(page =='Account Settings'){
      this.iab.create(`https://sitename.custhelp.com/app/account/profile`, `_blank`,{location:'yes', zoom:'yes'});
    }
    else if(page =='Login/Sign Up'){
      this.iab.create(`https://myjax.custhelp.com/app/utils/login_form`, `_system`);
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
