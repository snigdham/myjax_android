import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(private iab: InAppBrowser,) {

  }
  
  // will open in an inapp browser--->
  openLogin() {
    const browser =  this.iab.create(`https://opn-ast2.rightnowdemo.com/app/utils/login_form`, `_blank`,{toolbar: 'no', hideurlbar:'yes',location:'no'});

    browser.on('loadstart')
    .subscribe(
      (event) => {
      browser.executeScript({
        code: `alert("Hi");
        document.getElementById("rn_LoginForm_30_Submit").submit(function() {
          alert("Button code executed.");
        });
         `
      })
      browser.show();
      }
    );
    browser.on('loadstop')
    .subscribe(
      (event) => {
      browser.executeScript({
        code: ` 
        alert("loadcomplete");
        document.getElementById("rn_LoginForm_30_Submit").submit(function() {
          alert("Button code executed.");
        });   
        `
      })
      browser.show();
      }
    );
    // 
   
  //   this.iab.on('loadstop').subscribe(event: any => {
  //     iab.insertCSS({ code: "body{color: red;" });
  //  });https://Praveen:Password@1@opn-ast2.rightnowdemo.com/ci/admin
  }
  openSubmitRequestReport() {  
    this.iab.create(`https://myjax.custhelp.com/app/AllServices`, `_system`);
  }  

  openCheckStatus() {
  //https://Praveen:Password@1@opn-ast2.rightnowdemo.com/ci/admin
    this.iab.create(`https://myjax.custhelp.com/app/search_sr`, `_system`,{ hideurlbar:'no'});  }

  openSearch(searchText:any) {
    // https://opn-ast2.rightnowdemo.com/app/results/kw/
    this.iab.create(`https://myjax.custhelp.com/app/results/kw/`+searchText, `_system`);
  } 
 

  whatsAroundMeClicked() {
    //https://www.google.com/maps/search/things+to+do+near+me
    //https://www.google.com/search?q=things+to+do+near+me
    this.iab.create(`https://www.google.com/maps`, `_system`);
  }
 

}
