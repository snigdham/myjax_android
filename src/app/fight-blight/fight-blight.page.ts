import { Component, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-fight-blight',
  templateUrl: './fight-blight.page.html',
  styleUrls: ['./fight-blight.page.scss'],
})
export class FightBlightPage implements OnInit {

  constructor(private iab: InAppBrowser,) { }

  ngOnInit() {
    //https://opn-ast2.rightnowdemo.com/app
  }
  openReportBlight() {
    this.iab.create(`https://myjax.custhelp.com/app/AllFightBlight`, `_system`);
  }
  openScheduleCollection() {
    this.iab.create(`https://myjax.custhelp.com/app/Collection`, `_system`);
  }
  openRecyclingPickup() {
    this.iab.create(`https://myjax.custhelp.com/app/hauler`, `_system`);
  }
  openFlightBlightEvents() {
    this.iab.create(`http://www.coj.net/departments/neighborhoods/neighborhood-blight/events/all-events`, `_system`);
  }
  openExamplesOfBlight() {
   // https://opn-ast2.rightnowdemo.com/ci/admin
    this.iab.create(`http://www.coj.net/departments/neighborhoods/neighborhood-blight/examples-of-blight`, `_system`);
  }

}
