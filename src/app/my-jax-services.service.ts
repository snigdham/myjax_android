import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { HttpParams } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MyJaxServicesService {

  constructor( public http: HttpClient) { }
 
  public getXML() {    
    const _headers = new HttpHeaders();
    const headers = _headers.set('Content-Type', 'text/xml')
    let newsUrl = './assets/data/news.xml';
   // let newsUrl = 'https://cityofjacksonville.net/RSSFeed.aspx?ModID=76&CID=All-0';

    return this.http.get(newsUrl,{headers: _headers,responseType: 'text'})
      .pipe(tap(_ => console.log('getSearchResult completed')),
      catchError((error: HttpErrorResponse) => {
               return throwError(error);
           })
      );
  }
}
