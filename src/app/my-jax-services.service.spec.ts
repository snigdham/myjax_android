import { TestBed } from '@angular/core/testing';

import { MyJaxServicesService } from './my-jax-services.service';

describe('MyJaxServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyJaxServicesService = TestBed.get(MyJaxServicesService);
    expect(service).toBeTruthy();
  });
});
