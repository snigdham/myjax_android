import { Component, OnInit } from '@angular/core';
import { MyJaxServicesService } from '../my-jax-services.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgxXml2jsonService } from 'ngx-xml2json';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {
  constructor(private myJaxService: MyJaxServicesService,
    public http: HttpClient,
    private ngxXml2jsonService: NgxXml2jsonService,
    private iab: InAppBrowser,
    private HTTP: HTTP
  ) { }
  newsLists: any = [];

  ngOnInit() {
    this.callXmlData();
  }
  // Below are actual and dummy call, comment te method
  /// The One with actual url---> 
  //https://cityofjacksonville.net/RSSFeed.aspx?ModID=76&CID=All-0
  callXmlData() {
    this.HTTP.get('https://www.coj.net/CMSPages/coj_newsRss25.aspx', { responseType: '' }, {})
      .then(data => {
        // alert("in data"+data.status);
        // alert("in data"+data.data); // data received by server
        // alert("in data"+data.headers);
        const parser = new DOMParser();
        const xml = parser.parseFromString(data.data, 'text/xml');
        // const obj: any = this.ngxXml2jsonService.xmlToJson(xml);
        let x = xml.getElementsByTagName("title");
        var list = [];
        for (let i = 0; i < x.length; i++) {
          var obj = {};
          obj["title"] = xml.getElementsByTagName("title")[i].childNodes[0].nodeValue;
          obj["description"] = xml.getElementsByTagName("description")[i].childNodes[0].nodeValue;
          obj["link"] = xml.getElementsByTagName("link")[i].childNodes[0].nodeValue;
          list.push(obj);
        }
        this.newsLists = list;
        this.newsLists.shift();
      })
      .catch(error => {
        // error message as string--->   
        alert("err is:" + JSON.stringify(error.error));
      });
  }

  /// the working dummy one--->
  // callXmlData() {
  //   this.myJaxService.getXML()
  //     .subscribe(
  //       (data: any) => {
  //         console.log(data);
  //         //  parse the Xml now-->
  //         const parser = new DOMParser();
  //         const xml = parser.parseFromString(data, 'text/xml');
  //         //   const obj: any = this.ngxXml2jsonService.xmlToJson(xml);
  //         //   this.newsLists = obj.rss.channel.item;
  //         let x = xml.getElementsByTagName("title");
  // var list = [];
  // for (let i = 0; i < x.length; i++) {
  //   var obj = {};
  //   obj["title"] = xml.getElementsByTagName("title")[i].childNodes[0].nodeValue;
  //   obj["description"] = xml.getElementsByTagName("description")[i].childNodes[0].nodeValue;
  //   obj["link"] = xml.getElementsByTagName("link")[i].childNodes[0].nodeValue;
  //   list.push(obj);
  // }
  // this.newsLists = list;
  //         
  //       },
  //       (err: any) => { alert("err is:" + JSON.stringify(err)); }
  //     );
  // }

  openlink(link: any) {
    this.iab.create(link, `_system`, { location: 'yes', zoom: 'no' });
  }

  // the HTTP type of Solution---- This solution should only work with devices...
  //  this.HTTP.get('https://rss.news.yahoo.com/rss/topstories', {responseType: ''}, {})
  //   .then(data => {

  //     alert("in data"+data.status);
  //     alert("in data"+data.data); // data received by server
  //     alert("in data"+data.headers);

  //   })
  //   .catch(error => {

  //     alert(error.status);
  //     alert(error.error); // error message as string
  //     alert(error.headers);

  //   });
}
