(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-sign-up-login-sign-up-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/login-sign-up/login-sign-up.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/login-sign-up/login-sign-up.page.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <div style=\"width:100%; height:15px;background-image: url('assets/images/mobile-bg-topbar.png');margin-left:0px; \">\n    </div>\n    <table style=\"width:100%;text-align: center\">\n      <tr>\n        <td style=\"width:10%;\">\n          <ion-buttons slot=\"start\">\n            <ion-menu-button style=\"color:dodgerblue;\"></ion-menu-button>\n          </ion-buttons>\n        </td>\n        <td style=\"width:90%;\">\n          <img src=\"assets/images/mobile-logo-myjax.png\" />\n\n        </td>\n      </tr>\n    </table>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content style=\"text-align: left;\">\n  <div style=\"background-color:#E9E9E9 !important; \" >\n    <b><label style=\"font-size:15px;\">Please Login to continue</label></b>\n    <h3>Create an Account or <button style=\" margin-top:3%; background:transparent !important;color: dodgerblue; \n  border:1px solid gray; font-size: 25px;border-radius: 5px;\" (click)=\"showRegister=false;\">Log In</button></h3>\n    <div id=\"loginDiv\" style=\"width:100%;height:100%;\" *ngIf=\"!showRegister\">\n      <table width=\"100%\" style=\"margin-left:5%;\">\n        <tr>\n          <td>\n            <p>User Name *</p>\n            <input id=\"loginUserName\" name=\"loginUserName\" required minlength=\"4\" [(ngModel)]=\"login.loginUserName\"\n              #loginUserName=\"ngModel\">\n          </td>\n        </tr>\n        <tr>\n          <td>\n            <p>Password *</p>\n            <input id=\"loginPassword\" name=\"loginPassword\" class=\"form-control\" required minlength=\"4\"\n              [(ngModel)]=\"login.loginPassword\" #loginPassword=\"ngModel\">\n          </td>\n        </tr>\n\n        <tr>\n          <td style=\"width:100%;\">\n            <br />\n            <ion-button style=\"width:90%\">Log In</ion-button>\n          </td>\n        </tr>\n\n      </table>\n      <table width=\"100%\" style=\"text-align: center\">\n        <tr>\n          <td style=\"width:50%;\">\n            <a style=\"text-align: left\">Forgot password?</a>\n          </td>\n          <td style=\"width:50%;\">\n            <a style=\"align-self: right\" (click)=\"showRegister=true;\">Sign Up</a>\n          </td>\n        </tr>\n      </table>\n    </div>\n\n    <div id=\"signUpDiv\" *ngIf=\"showRegister\">\n      <table width=\"100%\" style=\"margin-left:5%;\">\n        <tr>\n          <td>\n            <p>Email Address *</p>\n            <input type=\"email\" id=\"emailAddress\" name=\"emailAddress\" required minlength=\"4\"\n              [(ngModel)]=\"register.emailAddress\" #emailAddress=\"ngModel\">\n          </td>\n        </tr>\n        <tr>\n          <td>\n            <p>Username *</p>\n            <input type=\"text\" class=\"textbox\" id=\"username\" name=\"username\" class=\"form-control\" required minlength=\"4\"\n              [(ngModel)]=\"register.username\" #username=\"ngModel\">\n          </td>\n        </tr>\n        <tr>\n          <td>\n            <p>Display Name *</p>\n            <input type=\"text\" id=\"displayname\" name=\"displayname\" class=\"form-control\" required minlength=\"4\"\n              [(ngModel)]=\"register.displayname\" #displayname=\"ngModel\">\n          </td>\n        </tr>\n        <tr>\n          <td>\n            <p>Password *</p>\n            <input type=\"password\" id=\"password\" name=\"password\" class=\"form-control\" required minlength=\"4\"\n              [(ngModel)]=\"register.password\" #password=\"ngModel\">\n          </td>\n        </tr>\n        <tr>\n          <td>\n            <p>Verify Password *</p>\n            <input type=\"text\" id=\"verifyPassword\" name=\"verifyPassword\" class=\"form-control\" required minlength=\"4\"\n              [(ngModel)]=\"register.verifyPassword\" #verifyPassword=\"ngModel\">\n          </td>\n        </tr>\n        <tr>\n          <td>\n            <p>FirstName *</p>\n            <input type=\"text\" id=\"firstName\" name=\"firstName\" class=\"form-control\" required minlength=\"4\"\n              [(ngModel)]=\"register.firstName\" #firstName=\"ngModel\">\n          </td>\n        </tr>\n        <tr>\n          <td>\n            <p>LastName *</p>\n            <input type=\"text\" id=\"lastName\" name=\"lastName\" class=\"form-control\" required minlength=\"4\"\n              [(ngModel)]=\"register.lastName\" #lastName=\"ngModel\">\n          </td>\n        </tr>\n        <tr>\n          <td></td>\n        </tr>\n        <tr>\n          <td>\n            <br />\n            <ion-button style=\"width:90%\">Register</ion-button>\n          </td>\n        </tr>\n      </table>\n\n    </div>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/login-sign-up/login-sign-up.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/login-sign-up/login-sign-up.module.ts ***!
  \*******************************************************/
/*! exports provided: LoginSignUpPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginSignUpPageModule", function() { return LoginSignUpPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_sign_up_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login-sign-up.page */ "./src/app/login-sign-up/login-sign-up.page.ts");







var routes = [
    {
        path: '',
        component: _login_sign_up_page__WEBPACK_IMPORTED_MODULE_6__["LoginSignUpPage"]
    }
];
var LoginSignUpPageModule = /** @class */ (function () {
    function LoginSignUpPageModule() {
    }
    LoginSignUpPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_login_sign_up_page__WEBPACK_IMPORTED_MODULE_6__["LoginSignUpPage"]]
        })
    ], LoginSignUpPageModule);
    return LoginSignUpPageModule;
}());



/***/ }),

/***/ "./src/app/login-sign-up/login-sign-up.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/login-sign-up/login-sign-up.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "input {\n  height: 35px;\n  border-radius: 3px;\n  width: 90%;\n  border-style: solid;\n  border-color: #BDBDBD !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4tc2lnbi11cC9DOlxcVXNlcnNcXHNuaWdkaGFtXFxteVByb2plY3RzXFxDT0pcXG15SmF4L3NyY1xcYXBwXFxsb2dpbi1zaWduLXVwXFxsb2dpbi1zaWduLXVwLnBhZ2Uuc2NzcyIsInNyYy9hcHAvbG9naW4tc2lnbi11cC9sb2dpbi1zaWduLXVwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGdDQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi1zaWduLXVwL2xvZ2luLXNpZ24tdXAucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW5wdXR7XHJcbiAgICBoZWlnaHQ6MzVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6M3B4O1xyXG4gICAgd2lkdGg6OTAlO1xyXG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcclxuICAgIGJvcmRlci1jb2xvcjogI0JEQkRCRCAhaW1wb3J0YW50OyAgXHJcbn0iLCJpbnB1dCB7XG4gIGhlaWdodDogMzVweDtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICB3aWR0aDogOTAlO1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItY29sb3I6ICNCREJEQkQgIWltcG9ydGFudDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/login-sign-up/login-sign-up.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/login-sign-up/login-sign-up.page.ts ***!
  \*****************************************************/
/*! exports provided: LoginSignUpPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginSignUpPage", function() { return LoginSignUpPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LoginSignUpPage = /** @class */ (function () {
    function LoginSignUpPage() {
        this.register = {};
        this.login = {};
        this.showRegister = true;
    }
    LoginSignUpPage.prototype.ngOnInit = function () {
    };
    LoginSignUpPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login-sign-up',
            template: __webpack_require__(/*! raw-loader!./login-sign-up.page.html */ "./node_modules/raw-loader/index.js!./src/app/login-sign-up/login-sign-up.page.html"),
            styles: [__webpack_require__(/*! ./login-sign-up.page.scss */ "./src/app/login-sign-up/login-sign-up.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LoginSignUpPage);
    return LoginSignUpPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-sign-up-login-sign-up-module-es5.js.map