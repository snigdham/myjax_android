(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["fight-blight-fight-blight-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/fight-blight/fight-blight.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/fight-blight/fight-blight.page.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n      <div style=\"width:100%; height:15px;background-image: url('assets/images/mobile-bg-topbar.png');margin-left:0px; \">\n      </div>\n      <table style=\"width:100%;text-align: left\">\n        <tr>\n          <td style=\"width:33%;\">\n            <ion-buttons slot=\"start\">\n              <ion-icon slot=\"icon-only\" style=\"color:dodgerblue;\" name=\"arrow-round-back\" role=\"img\" aria-label=\"star\"\n                routerLink=\"/home\" routerDirection=\"root\"></ion-icon>\n            </ion-buttons>\n          </td>\n          <td>\n            <img src=\"assets/images/mobile-logo-myjax.png\" />\n  \n          </td>\n        </tr>\n      </table>\n  \n    </ion-toolbar>\n  </ion-header>\n\n<ion-content style=\"text-align: center;\">\n  <div style=\"background-image: url('assets/images/MyJaxMobileApp_FightBlight_Assets/mobile-bg.jpg');width:100%;height:100%;text-align: center;\">\n<table style=\"width:100%; margin-top:5%;\">\n  <tr>\n    \n    <td style=\"width:60%;\">\n        <img style=\"vertical-align:middle;display:inline-block;margin-left:2%;\"\n        src=\"assets/images/MyJaxMobileApp_FightBlight_Assets/mobile-logo-fight-blight.png\">\n\n    </td>\n    <td style=\"width:40%;\">\n        <img style=\"vertical-align:middle;display:inline-block;margin-left:2%;\"\n        src=\"assets/images/MyJaxMobileApp_FightBlight_Assets/mobile-logo-dont-trash.png\">\n    </td>\n  </tr>\n</table>\n    <div style=\" position: fixed; bottom: 1%; width:100%;\">\n      <table style=\"width:96%;text-align: center; margin:2%; border-spacing:0 5px; border-collapse:separate;\">\n        <tr (click)=\"openReportBlight();\"\n          style=\"text-align:left; background-image: url('assets/images/mobile-icon-bg.png'); margin-left:1%;margin-right:1%; margin-top:2%; \">\n          <td>\n            <img style=\"vertical-align:middle;display:inline-block;margin-left:2%;\"\n              src=\"assets/images/MyJaxMobileApp_FightBlight_Assets/mobile-icon-report-take-picture.png\">\n\n          </td>\n          <td>\n            <p style=\"margin-left:2%; vertical-align:middle;display:inline-block;font-size: 15px;color:black\"><b>Report\n                blight and Take a picture</b></p>\n\n          </td>\n        </tr>\n        \n        <tr (click)=\"openScheduleCollection();\"\n          style=\" margin-top:2%;text-align:left; background-image: url('assets/images/mobile-icon-bg.png'); margin-left:1%;margin-right:1%; margin-top:2%; \">\n          <td>\n            <img style=\"vertical-align:middle;display:inline-block;margin-left:2%;\"\n              src=\"assets/images/MyJaxMobileApp_FightBlight_Assets/mobile-icon-schedule.png\">\n\n          </td>\n          <td>\n            <p style=\"margin-top:4%;margin-left:2%; vertical-align:middle;display:inline-block;font-size: 15px;color:black\">\n              <b>Schedule Collection for tires or appliances</b></p>\n\n          </td>\n        </tr>\n       \n        <tr (click)=\"openRecyclingPickup();\"\n          style=\" margin-top:2%;text-align:left; background-image: url('assets/images/mobile-icon-bg.png'); margin-left:1%;margin-right:1%; margin-top:2%; \">\n          <td>\n            <img style=\"vertical-align:middle;display:inline-block;\"\n              src=\"assets/images/MyJaxMobileApp_FightBlight_Assets/mobile-icon-dates.png\">\n\n          </td>\n          <td>\n            <p style=\"margin-top:3px;margin-left:2%;display:inline-block;font-size: 15px;color:black\"><b>Dates for\n                trash, yard waste and recycling pickup</b></p>\n\n          </td>\n        </tr>\n       \n        <tr (click)=\"openFlightBlightEvents();\"\n          style=\" margin-top:2%;text-align:left; background-image: url('assets/images/mobile-icon-bg.png'); margin-left:1%;margin-right:1%; margin-top:2%; \">\n          <td>\n            <img style=\"vertical-align:middle;display:inline-block;margin-left:2%;\"\n              src=\"assets/images/MyJaxMobileApp_FightBlight_Assets/mobile-icon-events.png\">\n\n          </td>\n          <td>\n            <p style=\"margin-left:2%; vertical-align:middle;display:inline-block;font-size: 15px;color:black\"><b>Flight\n                Blight Events</b></p>\n\n          </td>\n        </tr>\n        \n        <tr (click)=\"openExamplesOfBlight();\"\n          style=\" margin-top:2%;text-align:left; background-image: url('assets/images/mobile-icon-bg.png'); margin-left:1%;margin-right:1%; margin-top:2%; \">\n          <td>\n            <img style=\"vertical-align:middle;display:inline-block;margin-left:2%;\"\n              src=\"assets/images/MyJaxMobileApp_FightBlight_Assets/mobile-icon-examples.png\">\n\n          </td>\n          <td>\n            <p style=\"margin-left:2%; vertical-align:middle;display:inline-block;font-size: 15px;color:black\">\n              <b>Examples of Blight</b></p>\n\n          </td>\n        </tr>\n       \n      </table>\n    </div>\n  </div>\n</ion-content>"

/***/ }),

/***/ "./src/app/fight-blight/fight-blight.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/fight-blight/fight-blight.module.ts ***!
  \*****************************************************/
/*! exports provided: FightBlightPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FightBlightPageModule", function() { return FightBlightPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _fight_blight_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fight-blight.page */ "./src/app/fight-blight/fight-blight.page.ts");







var routes = [
    {
        path: '',
        component: _fight_blight_page__WEBPACK_IMPORTED_MODULE_6__["FightBlightPage"]
    }
];
var FightBlightPageModule = /** @class */ (function () {
    function FightBlightPageModule() {
    }
    FightBlightPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_fight_blight_page__WEBPACK_IMPORTED_MODULE_6__["FightBlightPage"]]
        })
    ], FightBlightPageModule);
    return FightBlightPageModule;
}());



/***/ }),

/***/ "./src/app/fight-blight/fight-blight.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/fight-blight/fight-blight.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZpZ2h0LWJsaWdodC9maWdodC1ibGlnaHQucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/fight-blight/fight-blight.page.ts":
/*!***************************************************!*\
  !*** ./src/app/fight-blight/fight-blight.page.ts ***!
  \***************************************************/
/*! exports provided: FightBlightPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FightBlightPage", function() { return FightBlightPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");



var FightBlightPage = /** @class */ (function () {
    function FightBlightPage(iab) {
        this.iab = iab;
    }
    FightBlightPage.prototype.ngOnInit = function () {
        //https://opn-ast2.rightnowdemo.com/app
    };
    FightBlightPage.prototype.openReportBlight = function () {
        this.iab.create("https://myjax.custhelp.com/app/AllFightBlight", "_system");
    };
    FightBlightPage.prototype.openScheduleCollection = function () {
        this.iab.create("https://myjax.custhelp.com/app/Collection", "_system");
    };
    FightBlightPage.prototype.openRecyclingPickup = function () {
        this.iab.create("https://myjax.custhelp.com/app/hauler", "_system");
    };
    FightBlightPage.prototype.openFlightBlightEvents = function () {
        this.iab.create("http://www.coj.net/departments/neighborhoods/neighborhood-blight/events/all-events", "_system");
    };
    FightBlightPage.prototype.openExamplesOfBlight = function () {
        // https://opn-ast2.rightnowdemo.com/ci/admin
        this.iab.create("http://www.coj.net/departments/neighborhoods/neighborhood-blight/examples-of-blight", "_system");
    };
    FightBlightPage.ctorParameters = function () { return [
        { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__["InAppBrowser"] }
    ]; };
    FightBlightPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-fight-blight',
            template: __webpack_require__(/*! raw-loader!./fight-blight.page.html */ "./node_modules/raw-loader/index.js!./src/app/fight-blight/fight-blight.page.html"),
            styles: [__webpack_require__(/*! ./fight-blight.page.scss */ "./src/app/fight-blight/fight-blight.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__["InAppBrowser"]])
    ], FightBlightPage);
    return FightBlightPage;
}());



/***/ })

}]);
//# sourceMappingURL=fight-blight-fight-blight-module-es5.js.map